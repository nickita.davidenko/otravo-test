# JS test answer
**Like**
* In class passed not the basic array, but it slice, for exclude aside effects.
* In class passed object with options, but not positioned arguments
* All values have self variables with good names, i.e. code maximum understood (but does not matter becouse uglify compress)

**Dislike**
* Using for loop in filter method(I would be use filter, if next element has specific type, then not accept current element)
* Some end of lines have `;`, but not in all
* About TT, what doing with NaN(it `number` type), with NaN, sorting will be broken

# Otravo test app
**Installation**
Docker must be installed in system.
You can build and run container with name what you want
> `$ docker build -t container_name .`

> `$ docker run -p 8080:8080 container_name`

Or you can use scripts in root project

> `start.sh` for linux based systems

> `start.bat` for windows

Them run server on address `localhost:8080`

**Access to bash**
> `docker ps` for see running containers and get container id

> `docker exec -i -t <container_id> /bin/bash` for run bash in container

**User story #1**

For use CLI, run bash in container .
> `python otravo.py -h ` -- for see help page

> `python otravo.py test.csv 2017-08-01 2017-08-15` -- for run search data using csv file

**User story #2**

After run container, navigate to http://localhost:8080
> Some configurations store at app.py file in config dictionary, I did can load from json or env variables, but I don't want.

**Testing**

Tests run when build container, but you can run them manual, for this run bash in container and use commands below
> `python -m unittest discover tests`

**After the end**

You can kill container
> `docker kill container_id`

# PS
I did can use docker-compose but for one app it overhead.