import unittest

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web
from app import app

TEST_PAGES = (
    ('GET', '/', 200),
    ('GET', '/?show_date=2018-06-13', 200),
    ('GET', '/test/', 404),
)

METHOD = 0
PATH = 1
STATUS = 2

class OtravoTestWebApp(AioHTTPTestCase):
    async def get_application(self):
        return app

    @unittest_run_loop
    async def test_routes(self):
        """
        All routes must response valid codes
        :return:
        """
        for page in TEST_PAGES:
            resp = await self.client.request(page[METHOD], page[PATH])
            assert resp.status == page[STATUS]

if __name__ == '__main__':
    unittest.main()