import unittest

import datetime

import os

from domains.show import ShowsManager, ShowStatusEnum

test_csv_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../test.csv')


class TestShowsS1(unittest.TestCase):
    def setUp(self):
        self.shows = ShowsManager(test_csv_file_path).get_shows(show_date=datetime.date(2017, 7, 1),
                                                                query_date=datetime.date(2017, 1, 1))

    def test_count(self):
        """
        Validate test shows count
        :return:
        """
        self.assertEqual(len(self.shows), 2, "with this requested dates inventory must have 2 shows")

    def test_status(self):
        """
        Validate test shows statuses
        :return:
        """
        shows_with_sale_not_started = list(
            filter(lambda s: s.status == ShowStatusEnum.SALE_NOT_STARTED.value, self.shows))
        self.assertEqual(len(shows_with_sale_not_started), 2, "shows must be with status SALE_NOT_STARTED")

    def test_left_tickets(self):
        """
        Validate test shows tickets left
        :return:
        """
        shows_with_200_tickets_left = list(
            filter(lambda s: s.tickets_left == 200, self.shows))
        self.assertEqual(len(shows_with_200_tickets_left), 2, "shows must be with 200 left tickets")

    def test_tickets_available(self):
        """
        Validate test shows tickets available
        :return:
        """
        shows_with_0_tickets_available = list(
            filter(lambda s: s.tickets_available == 0, self.shows))
        self.assertEqual(len(shows_with_0_tickets_available), 2, "shows must be with 0 available tickets")


class TestShowsS2(unittest.TestCase):
    def setUp(self):
        self.shows = ShowsManager(test_csv_file_path).get_shows(show_date=datetime.date(2017, 8, 15),
                                                                query_date=datetime.date(2017, 8, 1))

    def test_count(self):
        """
        Validate test shows count
        :return:
        """
        self.assertEqual(len(self.shows), 3, "with this requested dates inventory must have 3 shows")

    def test_status(self):
        """
        Validate test shows statuses
        :return:
        """
        shows_with_sale_not_started = list(
            filter(lambda s: s.status == ShowStatusEnum.OPEN_FOR_SALE.value, self.shows))
        self.assertEqual(len(shows_with_sale_not_started), 3, "shows must be with status OPEN_FOR_SALE")

    def test_left_tickets(self):
        """
        Validate test shows tickets left
        :return:
        """
        for show in self.shows:
            if show.title.lower() == 'cats':
                self.assertEqual(show.tickets_left, 50)
            elif show.title.lower() == 'comedy of errors' or show.title.lower() == 'everyman':
                self.assertEqual(show.tickets_left, 100)

    def test_tickets_available(self):
        """
        Validate test shows tickets available
        :return:
        """
        for show in self.shows:
            if show.title.lower() == 'cats':
                self.assertEqual(show.tickets_available, 5)
            elif show.title.lower() == 'comedy of errors' or show.title.lower() == 'everyman':
                self.assertEqual(show.tickets_available, 10)

    def test_genre(self):
        """
        Validate test shows genre
        :return:
        """
        for show in self.shows:
            if show.title.lower() == 'cats':
                self.assertEqual(show.genre, 'musical')
            elif show.title.lower() == 'comedy of errors':
                self.assertEqual(show.genre, 'comedy')
            elif show.title.lower() == 'everyman':
                self.assertEqual(show.genre, 'drama')


if __name__ == '__main__':
    unittest.main()
