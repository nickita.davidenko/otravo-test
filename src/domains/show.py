import csv
import datetime
import json
from enum import Enum
from typing import List
from functools import reduce

prices_by_genre = {
    'musical': 70,
    'comedy': 50,
    'drama': 40,
}


class ShowStatusEnum(Enum):
    OPEN_FOR_SALE = 'open for sale'
    SALE_NOT_STARTED = 'sale not started'
    SOLD_OUT = 'sold out'
    IS_OVER = 'is over'


class Show():
    """
    Class entity show.
    """
    date_format = '%Y-%m-%d'

    def __init__(self, title: str, opening_day: str, genre: str, query_date: datetime.date, show_date: datetime.date):
        self.title = title
        self.opening_day = datetime.datetime.strptime(opening_day, self.date_format).date()
        self.genre = genre.rstrip().lower()
        self.query_date = query_date
        self.show_date = show_date
        self.diff = (self.query_date - self.show_date).days
        self.show_day = (self.show_date - self.opening_day).days

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    @property
    def _max_amount(self):
        if self.show_day < 0 or self.show_day > 100:
            return 0
        elif self.show_day <= 60:
            return 200
        else:
            return 100

    @property
    def _max_tickets_per_day(self):
        if self.show_day < 0 or self.show_day > 100:
            return 0
        elif self.show_day <= 60:
            return 10
        else:
            return 5

    @property
    def tickets_left(self):
        if self.diff < -24:
            return self._max_amount
        if self.diff > -5:
            return 0
        return self._max_amount - ((24 + self.diff) * self._max_tickets_per_day)

    @property
    def tickets_available(self):
        return 0 if self.diff < - 24 or self.diff > -5 else self._max_tickets_per_day

    @property
    def status(self):
        if self.diff > 0:
            return ShowStatusEnum.IS_OVER.value
        elif self.diff > -5:
            return ShowStatusEnum.SOLD_OUT.value
        elif self.diff < -24:
            return ShowStatusEnum.SALE_NOT_STARTED.value
        else:
            return ShowStatusEnum.OPEN_FOR_SALE.value

    @property
    def price(self):
        if self.show_day < 0 or self.show_day > 100:
            return 0
        elif self.show_day > 80:
            return prices_by_genre[self.genre] * 0.8
        else:
            return prices_by_genre[self.genre]

    @property
    def json(self):
        return {"title": self.title,
                "tickets left": str(self.tickets_left),
                "tickets available": str(self.tickets_available),
                "status": self.status}


class ShowsManager():
    def __init__(self, path_to_file):
        self._path_to_file = path_to_file

    def get_shows_in_json(self, show_date: datetime.date, query_date: datetime.date):
        """
        Return json representation filtered show
        :param show_date:
        :param query_date:
        :return:
        """
        filtered_shows = self.get_shows(show_date, query_date)
        genres = list(set([show.genre for show in filtered_shows]))
        dict_repr = {"inventory": []}
        for genre in genres:
            genre_dict = {'genre': genre, "shows": [show.json for show in filtered_shows if show.genre == genre]}
            dict_repr["inventory"].append(genre_dict)
        return json.dumps(dict_repr, indent=4)

    def get_shows(self, show_date: datetime.date, query_date: datetime.date = datetime.date.today(), ) -> List[
        Show]:
        """
        Filter shows by preset dates.
        If preset show_date between opening_day and opening_day + 100 days then this show fit.
        :param query_date:
        :param show_date:
        :return: shows in preset date
        """
        shows = self._read_from_file(query_date=query_date, show_date=show_date)
        shows_by_show_day = list(
            filter(lambda show: show.opening_day <= show_date <= show.opening_day + datetime.timedelta(days=100),
                   shows))
        return shows_by_show_day

    def _read_from_file(self, query_date: datetime.date, show_date: datetime.date) -> List[Show]:
        """
        Make list shows with need params
        :param query_date:
        :param show_date:
        :return:
        """
        shows = []
        with open(self._path_to_file) as shows_csv:
            shows_reader = csv.reader(shows_csv, delimiter=',')
            show_rows = list(
                filter(lambda row: ''.join(row).rstrip(), (row for row in shows_reader)))  # clean blank lines
            for show_row in show_rows:
                shows.append(Show(*show_row, query_date=query_date, show_date=show_date))
        return shows
