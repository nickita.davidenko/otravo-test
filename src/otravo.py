import sys

import datetime

from domains.show import ShowsManager

date_format = '%Y-%m-%d'


def inventory(file_path, query_date, show_date):
    try
        query_date = datetime.datetime.strptime(query_date, date_format).date()
        show_date = datetime.datetime.strptime(show_date, date_format).date()
        print(ShowsManager(file_path).get_shows_in_json(query_date=query_date, show_date=show_date))
    except Exception as e:
        print("Something went wrong, please check argument values, for help use -h flag")
        

if __name__ == '__main__':
    if '-h' in sys.argv or len(sys.argv) < 3:
        help_text = """
        OTRAVO CLI TOOL
    How to use:
    otravo.py [path/to/file.csv] [query date in format YYYY-MM-DD] [show date in format YYYY-MM-DD]
    or
    otravo.py -h for see this page      
        """
        print(help_text)
    else:
        inventory(*sys.argv[1:])
