import os
from aiohttp import web
import aiohttp_jinja2
import jinja2

from handlers.index import ShowsView

current_file_path = os.path.dirname(os.path.abspath(__file__))

config = {
    'shows_csv_path': os.path.join(current_file_path, 'shows.csv'),
    'date_format': '%Y-%m-%d',
}

app = web.Application()

app.router.add_route('*', '/', ShowsView)

aiohttp_jinja2.setup(
    app, loader=jinja2.FileSystemLoader(os.path.join(current_file_path, 'templates')))

app['config'] = config


if __name__ == '__main__':
    web.run_app(app, host='0.0.0.0', port=8080)
