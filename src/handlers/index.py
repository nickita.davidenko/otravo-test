import aiohttp_jinja2
import datetime
from aiohttp import web

from domains.show import ShowsManager


class ShowsView(web.View):
    @aiohttp_jinja2.template('index.html')
    async def get(self):
        date_format = self.request.app['config']['date_format']
        form_show_date = self.request.rel_url.query.get('show_date', str(datetime.date.today()))
        show_date = datetime.datetime.strptime(form_show_date, date_format).date()
        shows = ShowsManager(path_to_file=self.request.app['config']['shows_csv_path']).get_shows(show_date=show_date,
                                                                                                  query_date=datetime.date.today())
        return dict(shows=shows,
                    form_show_date=form_show_date)
