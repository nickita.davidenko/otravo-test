FROM python:3.6

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip install --no-cache-dir -r requirements.txt

COPY src/ /usr/src/app
COPY shows.csv /usr/src/app
COPY test.csv /usr/src/app
RUN python -m unittest discover tests

ENTRYPOINT ["python", "app.py"]