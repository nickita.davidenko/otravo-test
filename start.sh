#!/usr/bin/env bash

docker stop $(docker ps -a -q --filter ancestor=otravo --format="{{.ID}}")
docker build -t otravo .
docker run -p 8080:8080 otravo